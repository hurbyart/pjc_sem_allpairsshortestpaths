**Goal**

The goal was creating C++ program which calculates all-pairs shortest paths in directed graph. Different types of input and output should be used. The program must calculate all node pairs shortest paths if path exist and then print path and its length.

**Implementation**

Program checks provided arguments. There are different types of arguments:

--help  displays base program instructions and finishes program

-fi <"file name">  uses input file with graph instead of stdin

-fo <"file name"> writes output to file instead of stdout

-fio <"file name 1"> <"file name 2">  uses input file with graph instead of stdin and writes output to file instead of stdout

In both cases with input from file or from stdin input must be in such form:

![](pictures/inputForm.png)


The program uses the Floyd-Warshall algorithm for path calculating. There is only a single-thread implementation. There different program return codes:

0 – everything is okay

1 – help program argument was used

2 – read file problem

Also errors are thrown in case of unexpected program arguments.
The output program in both cases with output to file or to stdout is presented in the such form:

![](pictures/outputForm.png)

Program arguments example: 

-fio data/data04.txt output04.txt

Note: input/output files will be located in "cmake-build-debug" directory as the program runs from this directory.