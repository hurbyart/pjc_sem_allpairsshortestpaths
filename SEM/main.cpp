#include <iostream>
#include <bits/stdc++.h>

using namespace std;

/**
 * Handles program arguments. Argument types are described in main function.
 *
 * @param argc - number of arguments
 * @param argv - arguments
 * @return string array with 2 values: names of input and output files if such exists or word "absent" otherwise
 */
string* handleUnusualArgumentsAmount(int argc, char *argv[]){
    auto* inputOutputFiles = new string[2];
    inputOutputFiles[0] = "absent";
    inputOutputFiles[1] = "absent";
    if(argc == 4){
        if(strcmp(argv[1], "-fio") == 0){
            inputOutputFiles[0] = argv[2];
            inputOutputFiles[1] = argv[3];
        } else {
            throw runtime_error("Unexpected program arguments!");
        }
    }else if(argc == 3){
        if(strcmp(argv[1], "-fi") == 0){
            inputOutputFiles[0] = argv[2];
        } else if(strcmp(argv[1], "-fo") == 0){
            inputOutputFiles[1] = argv[2];
        } else {
            throw runtime_error("Unexpected program arguments!");
        }
    } else if(argc == 2 && strcmp(argv[1], "--help") == 0){
        cout << "This program calculates all node pairs shortest paths.\nTo try it run it without program arguments and "
                "fill in graph using form documentation or use program arguments -fi, -fo, -fio with file names to use "
                "file input/output instead of stdin/stdout";
        exit(1);
    } else {
        throw runtime_error("Unexpected program arguments!");
    }
    return inputOutputFiles;
}

/**
 * Creates distance matrix using input from stdin
 *
 * @return distances matrix created from stdin
 */
vector<vector<int>> createDistancesMatrixFromStdin(){
    int vertexAmount, edgeAmount;
    cin >> vertexAmount >> edgeAmount;
    vector<vector<int>> distancesMatrix(vertexAmount, vector<int>(vertexAmount, INT_MAX));
    for(int i = 0; i < edgeAmount; i++){
        int from, to, length;
        cin >> from >> to >> length;
        distancesMatrix[from][to] = length;
    }
    return distancesMatrix;
}

void parseFileLineAndStoreInDistanceMatrix(vector<string> line, vector<vector<int>> &matrix){
    int from = stoi(line[0]);
    int to = stoi(line[1]);
    int length = stoi(line[2]);
    matrix[from][to] = length;
}


/**
 * Creates distance matrix using input file
 *
 * @param fileName input file name
 * @return distances matrix created from input file
 */
vector<vector<int>> createDistancesMatrixFromFile(const string& fileName){
    string line;
    ifstream container_file(fileName);
    vector<vector<int>> distancesMatrix;
    int vertexAmount, edgeAmount;

    if (container_file.is_open()) {
        if(getline(container_file, line)){
            istringstream iss(line);
            vector<string> firstLineString{ istream_iterator<string>(iss), {} };
            vertexAmount = stoi(firstLineString[0]);
            edgeAmount = stoi(firstLineString[1]);
            distancesMatrix = vector<vector<int>>(vertexAmount, vector<int>(vertexAmount, INT_MAX));
        } else {
            exit(2);
        }
        for(int i = 0; i < edgeAmount; i++){
            getline(container_file, line);
            istringstream iss(line);
            vector<string> lineString{ istream_iterator<string>(iss), {} };
            parseFileLineAndStoreInDistanceMatrix(lineString, distancesMatrix);
        }
        container_file.close();
    } else {
        exit(2);
    }
    return distancesMatrix;
}

/**
 * Allows using + to merge 2 vectors
 *
 * @tparam T
 * @param v1
 * @param v2
 * @return
 */
template<typename T>
vector<T> operator+(const vector<T>& oldVector1, const vector<T>& oldVector2){
    vector<T> newVector(begin(oldVector1), end(oldVector1));
    newVector.insert(end(newVector), begin(oldVector2), end(oldVector2));
    return newVector;
}

/**
 * Calculates all to all paths in graph using Floyd-Warshall algorithm
 *
 * @param matrix distance matrix
 * @return for every pair of nodes returns vector with path between these nodes if such exists
 */
vector<vector<vector<int>>> floydWarshallAlgorithm(const vector<vector<int>>& matrix){
    unsigned long vertexAmount = matrix.size();
    vector<vector<int>> distances(vertexAmount, vector<int>(vertexAmount));
    vector<vector<vector<int>>> paths(vertexAmount, vector<vector<int>>(vertexAmount, vector<int>()));
    for(int i = 0; i < vertexAmount; i++)
        for(int j = 0; j < vertexAmount; j++) {
            distances[i][j] = matrix[i][j];
            if (matrix[i][j] != INT_MAX) paths[i][j].push_back(j);
        }

    for(int i = 0; i < vertexAmount; i++){
        for(int j = 0; j < vertexAmount; j++){
            for(int k = 0; k < vertexAmount; k++){
                if(distances[j][k] > distances[j][i] + distances[i][k] && distances[i][k] != INT_MAX && distances[j][i] != INT_MAX && j != k){
                    distances[j][k] = distances[j][i] + distances[i][k];
                    paths[j][k] = paths[j][i] + paths[i][k];
                }
            }
        }
    }
    return paths;
}

/**
 *
 * @param stream cout or stream to file in case of using output to file option
 * @param paths paths after Floyd-Warshall algorithm
 * @param distanceMatrix distance matrix from input
 */
void printResultToStream(ostream& stream, const vector<vector<vector<int>>>& paths, const vector<vector<int>>& distanceMatrix){
    stream << "paths:" << endl;
    for(int i = 0; i < paths.size(); i++){
        for(int j = 0; j < paths[i].size(); j++){
            if(!paths[i][j].empty()) {
                int distance = 0;
                int prev = i;
                stream << "{" << i <<" -> " << j << "}: " << i << "->";
                for (int k = 0; k < paths[i][j].size(); k++){
                    stream << paths[i][j][k];
                    if(k != paths[i][j].size() - 1) stream << "->";
                    distance += distanceMatrix[prev][paths[i][j][k]];
                    prev = paths[i][j][k];
                }
                stream << "   | distance: " << distance << endl;
            }
        }
    }
}


/**
 * Starts function for output writing. In case of output file creates stream for file.
 *
 * @param fileName output file name
 * @param paths paths after Floyd-Warshall algorithm
 * @param distanceMatrix distance matrix from input
 */
void printResult(const string& fileName, const vector<vector<vector<int>>>& paths, const vector<vector<int>>& distanceMatrix){
    if(strcmp(fileName.c_str(), "absent") == 0){
        printResultToStream(cout, paths, distanceMatrix);
    } else {
        ofstream output(fileName);
        printResultToStream(output, paths, distanceMatrix);
    }
}

/**
 * arguments:
 * --help for program description
 * -fi <fileName> for using file input instead of srdin
 * -fo <fileName> for output to file instead of stdout
 * -fio <fileName1> <fileName2> for input from file and output to file
 *
 * @param argc - number of arguments
 * @param argv - arguments
 * @return 0 in case program has been finished without errors, 1 in case help argument was used, 2 in case of file read problem
 */
int main(int argc, char *argv[]) {
    string* inputOutputFiles = nullptr;
    if(argc > 1) inputOutputFiles = handleUnusualArgumentsAmount(argc, argv);

    vector<vector<int>> distancesMatrix;
    if(inputOutputFiles == nullptr || strcmp(inputOutputFiles[0].c_str(), "absent") == 0) distancesMatrix = createDistancesMatrixFromStdin();
    else distancesMatrix = createDistancesMatrixFromFile(inputOutputFiles[0]);

    vector<vector<vector<int>>> paths = floydWarshallAlgorithm(distancesMatrix);

    if(inputOutputFiles == nullptr) printResult("absent", paths, distancesMatrix);
    else printResult(inputOutputFiles[1], paths, distancesMatrix);

    return 0;
}
